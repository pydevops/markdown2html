## A Bash script to converter a list of markdown (.md) files to html. 
## Install pandoc 
[How to install pandoc](http://johnmacfarlane.net/pandoc/installing.html)
## Run
./md2html.sh

## Add as cron job 
crontab -e
