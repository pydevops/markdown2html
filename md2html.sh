#!/bin/bash

SRC_DIR=$HOME/Desktop
DEST_DIR=$HOME/man

mkdir -p $DEST_DIR
for mdfile in ${SRC_DIR}/*.md
do
  filename=${mdfile##*/}
  basename=${filename%.md}
  pandoc -o ${DEST_DIR}/${basename}.html ${mdfile}
done
